<article id="colophon">

    <h2>COLOPHON</h2>

    <dl class="bible">

    <dt>Éditeur responsable</dt><dd style="padding-top: 10px;">Fabien Dehasseler — avenue Félix Marchal 1, 1030 Bruxelles</dd>

    <dt>Photographie</dt>

    <dd>Pages 6–7, 22–23, 30–31&#x202F;: Hichem&nbsp;Dahes<br/>

    Page 10&#x202F;: Simone Wijckaert-Inion<br/>

    Pages 12 et 14&#x202F;: Danièle Pierre</dd>

    <dt>Design graphique</dt><dd>Open Source Publishing</dd>

    <dt>Outils et programmes</dt><dd style="padding-top: 10px;">html2print, laidout, Inkscape, Gimp, Git, Etherpad, GNU/Linux</dd>

    <dt>Typographies</dt><dd>Famille UmeLoop, générée à partir de la famille Ume, avec la recette Fons et le logiciel GlyphTracer1.4</dd>

    <dt>Fichiers disponibles</dt><dd style="padding-top: 10px;">sous licence art libre sur &lt;osp.kitchen/work/balsamine.2015-2016&gt;</dd>

    <dt>Impression</dt><dd>Imprimerie Gillis, Bruxelles</dd>

    </dl>

</article>


